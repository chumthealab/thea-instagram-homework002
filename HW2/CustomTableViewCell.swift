//
//  CustomTableViewCell.swift
//  HW2
//
//  Created by Chum Thea on 11/21/19.
//

import UIKit

class CustomTableViewCell: UITableViewCell {

    @IBOutlet weak var likeLabel: UILabel!
    @IBOutlet weak var userNameLabel: UILabel!

    @IBOutlet weak var moreButton: UIButton!
    @IBOutlet weak var profilePhoto: UIImageView!
    @IBOutlet weak var captionLabel: UILabel!
    @IBOutlet weak var photo: UIImageView!
    @IBOutlet weak var likeButton: UIButton!
    @IBOutlet weak var commentButton: UIButton!
    @IBOutlet weak var shareButton: UIButton!
    @IBOutlet weak var saveButton: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
    
           // MARK: - Customize Post Photo
           photo.frame.size.width = self.frame.size.width
           photo.frame.size.height = self.frame.size.height - 220
           photo.clipsToBounds = true
           photo.contentMode = .scaleAspectFill
           
           likeLabel.font = UIFont.preferredFont(forTextStyle: .headline) // bold
           
           customProfilePhoto()
           
           customSocialButtons(likeButton)
           customSocialButtons(commentButton)
           customSocialButtons(shareButton)
           customSocialButtons(saveButton)
           customSocialButtons(moreButton)
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
       
        
    }
    
    func customProfilePhoto(){
        profilePhoto.frame.size.width = 40
        profilePhoto.frame.size.height = 40
        
        profilePhoto.clipsToBounds = true
        profilePhoto.layer.cornerRadius = profilePhoto.frame.size.width / 2
//        profilePhoto.contentMode = .scaleAspectFit
        profilePhoto.contentMode = .scaleAspectFill

    }
    
    func customSocialButtons(_ btn: UIButton){
        btn.contentMode = .center
        btn.imageView?.contentMode = .scaleToFill
        btn.imageView?.tintColor = .black
        btn.addTarget(self, action: #selector(self.socialBtnPressed(_:)), for: .touchUpInside)
    }
    
    @objc func socialBtnPressed(_ sender: UIButton){
        print(sender.titleLabel?.text)
    }

}
