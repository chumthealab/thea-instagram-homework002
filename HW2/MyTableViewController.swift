//
//  MyTableViewController.swift
//  HW2
//
//  Created by Chum Thea on 11/21/19.
//

import UIKit

class MyTableViewController: UITableViewController {
    
    var posts = [
            Post(userName: "shin_chan", userProfilePic: "profile1.jpg", imageURL: "shinchan1.jpg", numberOfLike: 221100, caption: "No matter how old i get in, i will never stop watching Shin Chan"),
            Post(userName: "nene_chan", userProfilePic: "profile2.jpg", imageURL: "shinchan2.jpg", numberOfLike: 1100, caption: "Sorry, I gonna be busy picking my nose or something"),
            Post(userName: "masao_kun", userProfilePic: "profile3.jpg", imageURL: "shinchan3.jpg", numberOfLike: 1, caption: "I never make mistake twice, i make it 5 or 6 times just to be sure."),
            Post(userName: "kasama_kun", userProfilePic: "profile4.jpg", imageURL: "shinchan4.jpg", numberOfLike: 10400000, caption: "Because i hurt you, i hurt myself too. i am sorry")
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.showsHorizontalScrollIndicator = false
        tableView.showsVerticalScrollIndicator = false
        
        tableView.separatorColor = .clear
        
        customNavigationItem()
    }
    
    func customNavigationItem(){
        navigationController?.navigationBar.barTintColor = .white
         let instaImage = UIImage(named: "instagram")
        
         let instagramImageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 20, height: 40))
        
         instagramImageView.image = instaImage
        instagramImageView.contentMode = .scaleAspectFit
         navigationItem.titleView = instagramImageView
        
        let cameraBarBtnItem = UIBarButtonItem(image: UIImage(systemName: "camera"), style: .plain, target: nil, action: nil)
        cameraBarBtnItem.tintColor = .black
        
        let planeBarBtnItem = UIBarButtonItem(image: UIImage(systemName: "paperplane"), style: .plain, target: nil, action: nil)
        planeBarBtnItem.tintColor = .black
        
        
         navigationItem.leftBarButtonItem = cameraBarBtnItem
         navigationItem.rightBarButtonItem = planeBarBtnItem
         
    }

    // MARK: - Table view data source
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        self.view.frame.height * 0.45
    }
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return posts.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! CustomTableViewCell

        let post = posts[indexPath.row]
        cell.userNameLabel.text = post.userName
        cell.userNameLabel.numberOfLines = 0
        
        cell.profilePhoto.image = UIImage(named: post.userProfilePic)
        cell.photo.image = UIImage(named: post.imageURL)
        cell.likeLabel.text = "\(post.numberOfLike.abbreviationNum()) \(post.numberOfLike == 1 ? "Like":"Likes")"
        cell.captionLabel.text = post.caption
        cell.separatorInset = .zero

        return cell
    }
}


// MARK: - Custom Abbreviation Number for like
extension Int{
    func abbreviationNum() -> String{
        if self < 1000 {
            return "\(self)"
        }else if self < 1000000 {
            var n = Double(self)
            n = Double(floor(n/100)/10)
            return "\(n.description)K"
        }else {
            var n = Double(self)
            n = Double(floor(n/10000) / 100)
            return "\(n.description)M"
        }
    }
}

// MARK: - Structure Post
struct Post {
    var userName: String
    var userProfilePic: String
    var imageURL: String
    var numberOfLike: Int
    var caption: String
}
